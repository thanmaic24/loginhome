import LoginHomeConstants from "../constants/index";

const initialState = {
  homeData: [],
  isLoading: true,
  isError: false
};

function loginHomeReducer(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case LoginHomeConstants.LOGIN_HOME:
      return { ...state, isLoading: true, homeData: [...payload] };

    case LoginHomeConstants.LOGIN_HOME_SUCCESS:
      return { ...state, isLoading: false, homeData: [...payload] };

    case LoginHomeConstants.LOGIN_HOME_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true
      };

    default:
      return state;
  }
}

export default loginHomeReducer;
