import { combineReducers } from "redux";
import loginHomeReducer  from "./loginHomeReducer";

export const reducer = combineReducers({
	loginHome: loginHomeReducer,
});