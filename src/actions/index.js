import LoginHomeConstants from "../constants";

export const loginHome = () => {
    return {
      type: LoginHomeConstants.LOGIN_HOME_SUCCESS,
      payload: data
    };
  };


export const loginHomeSuccess = data => {
  return {
    type: LoginHomeConstants.LOGIN_HOME_SUCCESS,
    payload: data
  };
};

export const loginHomeError = data => {
  return {
    type: LoginHomeConstants.LOGIN_HOME_ERROR,
    payload: data
  };
};
