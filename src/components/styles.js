import { StyleSheet, Dimensions, Platform } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 5
  },
  firstpart: {
    flex: 0.3,
    // backgroundColor: "red",
    alignItems: "center",
    padding: 10
  },
  secondpart: {
    flex: 0.6,
    // backgroundColor: "pink",
    paddingHorizontal: 10,
    justifyContent: "center"
  },
  thirdpart: {
    flex: 0.1,
    // backgroundColor: "red",
    justifyContent: "center",
    alignItems: "center"
  },
  rateStar: {
    flexDirection: "row",
    marginTop: 5,
    alignItems: "center",
    justifyContent: "space-between"
  },
  listText: {
    fontSize: 15,
    color: "#00b5ec"
  },
  listText2: {
    fontSize: 15,
    color: "grey"
  }
});
export default styles;
