import React from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import styles from "./styles";

const ListItem = props => {
  return (
    <View style={styles.container}>
      <View style={styles.firstpart}>
        <Image
          style={{ width: 80, height: 60 }}
          source={{
            uri: "https://pngimage.net/wp-content/uploads/2018/05/default-image-png-2.png"
          }}
        />
      </View>
      <View style={styles.secondpart}>
        <Text style={styles.listText}>{props.itemData.name}</Text>
        <View>
          <Text style={styles.listText2}>{props.itemData.age}</Text>
          <Text style={styles.listText2}>{props.itemData.gender}</Text>
          <Text style={styles.listText2}>{props.itemData.email}</Text>
          <Text style={styles.listText2}>{props.itemData.phoneNo}</Text>
        </View>
      </View>
    </View>
  );
};

ListItem.defaultProps = {};

ListItem.propTypes = {};

export default ListItem;
