import React from "react";
import {
  StyleSheet,
  View,
  Text,
  Button,
  FlatList,
  ActivityIndicator
} from "react-native";
import ListItem from "../components/ListItem";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { loginHomeSuccess, loginHomeError } from "../actions/index";
import { get } from "lodash";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam("email", "no id")
    };
  };

  async componentDidMount() {
    await fetch(
      "http://www.mocky.io/v2/5cab42983000007817904a04"
    )
      .then(response => response.json())
      .then(responseJson => {
        // console.log("responseJson", responseJson.user);
        this.props.loginHomeSuccess(responseJson.user);
      })
      .catch(error => {
        this.props.loginHomeError();
      });
  }

  _renderItem = ({ item }) => <ListItem id={item.id} itemData={item} />;

  _keyExtractor = (item, index) => item.id;

  render() {
    const data = get(this.props.loginHomeProps, "homeData");
    const isLoading = get(this.props.loginHomeProps, "isLoading");
    const isError = get(this.props.loginHomeProps, "isError");
    return (
      <View style={styles.container}>
        {isError ? (
          <View style={styles.centerItem}>
            <Text>Something went wrong!</Text>
          </View>
        ) : null}
        {isLoading ? (
          <View style={styles.centerItem}>
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
        ) : null}
        <FlatList
          data={data}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          ItemSeparatorComponent={() => <View style={styles.seperator} />}
          ListFooterComponent={() => <View style={styles.extra} />}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f5f5f5"
  },
  seperator: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: "black"
  },
  extra: { backgroundColor: "#f5f5f5", height: 20 },
  centerItem: { justifyContent: "center", alignItems: "center", flex: 1 }
});

const mapStateToProps = state => ({
  loginHomeProps: state.loginHome
});

const mapDispatchToProps = dispatch => ({
  loginHomeSuccess: bindActionCreators(loginHomeSuccess, dispatch),
  loginHomeError: bindActionCreators(loginHomeError, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
