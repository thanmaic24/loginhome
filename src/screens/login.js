import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  Image,
  Alert
} from "react-native";
import styles from "./styles.js";
import constants from "../constants";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }

  onSubmit = () => {
    const { email, password } = this.state;
    const validateEmail = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(email);
    const validatePassword = /(?=.*\d)(?=.*[a-z]).{6,}/.test(password);
    if (
      validateEmail &&
      validatePassword &&
      email === constants.EMAILID &&
      password === constants.PASSWORD
    ) {
      this.props.navigation.navigate("Home", {
        email: this.state.email
      });
    } else {
      alert(
        "Please enter valid email and password with atleast 6 characters which includes atleast one digit and letter"
      );
    }
  };

  render() {
    const { email, password } = this.state;
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={styles.container}>
          <Image
            style={styles.trianz}
            source={{
              uri:
                "https://res-5.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_256,w_256,f_auto,q_auto:eco/v1397188576/267f5f6980e462c89af38b89c5d0a353.jpg"
            }}
          />
          <View style={styles.inputContainer}>
            <Image
              style={styles.inputIcon}
              source={{
                uri: "https://png.icons8.com/message/ultraviolet/50/3498db"
              }}
            />
            <TextInput
              style={styles.inputs}
              placeholder="Email"
              keyboardType="default"
              underlineColorAndroid="transparent"
              onChangeText={email => this.setState({ email })}
              returnKeyType="next"
              value={email}
            />
          </View>

          <View style={styles.inputContainer}>
            <Image
              style={styles.inputIcon}
              source={{
                uri: "https://png.icons8.com/key-2/ultraviolet/50/3498db"
              }}
            />
            <TextInput
              style={styles.inputs}
              placeholder="Password"
              secureTextEntry={true}
              underlineColorAndroid="transparent"
              onChangeText={password => this.setState({ password })}
              returnKeyType="done"
              value={password}
            />
          </View>

          <TouchableOpacity
            style={[styles.buttonContainer, styles.loginButton]}
            onPress={this.onSubmit}
          >
            <Text style={styles.loginText}>Login</Text>
          </TouchableOpacity>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
