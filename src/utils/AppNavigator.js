import { createStackNavigator, createAppContainer } from "react-navigation";
import Home from "../screens/home";
import Login from "../screens/login";

const AppNavigator = createStackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: {
        header: null
      }
    },
    Home: { screen: Home }
  },
  {
    initialRouteName: "Login"
  }
);

export default createAppContainer(AppNavigator);
